import test from 'ava';
import { sortResults } from 'js/actions/searchActions'
import configureStore from 'redux-mock-store'
import thunkMiddleware from 'redux-thunk'

// expect that the results are sorted ASC and and props changes to DESC for next sorting
test('sortResults action', t => {

    return new Promise((resolve, reject) => {
        const dataUnsorted = [{ "name": "hammer", "platform": "Bower", "description": "A javascript library for multi-touch gestures :// You can touch this", "homepage": "http://hammerjs.github.io", "repository_url": "https://github.com/EightMedia/hammer.js.git", "normalized_licenses": ["MIT"], "rank": 14, "latest_release_published_at": "2016-04-22T16:14:36.000Z", "latest_release_number": "v2.0.8", "language": "JavaScript", "status": null, "package_manager_url": null, "stars": 15179, "forks": 2201, "keywords": ["touch", "gestures"], "latest_stable_release": { "id": 34179256, "github_repository_id": 19730, "name": "v2.0.8", "sha": "ee611316bec077fcfbba3fd604ebc4b0b35ac288", "kind": "tag", "published_at": "2016-04-22T16:14:36.000Z", "created_at": "2016-04-22T16:14:51.514Z", "updated_at": "2016-04-22T16:14:51.514Z" }, "versions": [] }, { "name": "hammerjs-compatible", "platform": "Bower", "description": "The hammerjs-compatible is utility for use hammerjs on Internet Explorer 8.", "homepage": "", "repository_url": "https://github.com/naver/hammerjs-compatible.git", "normalized_licenses": ["MIT"], "rank": 2, "latest_release_published_at": "2016-11-10T07:16:16.296Z", "latest_release_number": null, "language": "JavaScript", "status": null, "package_manager_url": null, "stars": 2, "forks": 1, "keywords": ["hammer", "hammer.js", "compatible", "ie8", "egjs"], "latest_stable_release": null, "versions": [] }, { "name": "hammerjs-ts", "platform": "Bower", "description": "TypeScript declarations file taken from DefinitelyTyped for use in Bower. ", "homepage": null, "repository_url": "https://github.com/riaform/hammerjs-ts.git", "normalized_licenses": [], "rank": 0, "latest_release_published_at": "2016-11-09T17:50:10.082Z", "latest_release_number": null, "language": "TypeScript", "status": null, "package_manager_url": null, "stars": 0, "forks": 0, "keywords": [], "latest_stable_release": null, "versions": [] }];

        const data = [
            {
                "name": "hammerjs-ts",
                "platform": "Bower",
                "description": "TypeScript declarations file taken from DefinitelyTyped for use in Bower. ",
                "homepage": null,
                "repository_url": "https://github.com/riaform/hammerjs-ts.git",
                "normalized_licenses": [

                ],
                "rank": 0,
                "latest_release_published_at": "2016-11-09T17:50:10.082Z",
                "latest_release_number": null,
                "language": "TypeScript",
                "status": null,
                "package_manager_url": null,
                "stars": 0,
                "forks": 0,
                "keywords": [

                ],
                "latest_stable_release": null,
                "versions": [

                ]
            },

            {
                "name": "hammerjs-compatible",
                "platform": "Bower",
                "description": "The hammerjs-compatible is utility for use hammerjs on Internet Explorer 8.",
                "homepage": "",
                "repository_url": "https://github.com/naver/hammerjs-compatible.git",
                "normalized_licenses": [
                    "MIT"
                ],
                "rank": 2,
                "latest_release_published_at": "2016-11-10T07:16:16.296Z",
                "latest_release_number": null,
                "language": "JavaScript",
                "status": null,
                "package_manager_url": null,
                "stars": 2,
                "forks": 1,
                "keywords": [
                    "hammer",
                    "hammer.js",
                    "compatible",
                    "ie8",
                    "egjs"
                ],
                "latest_stable_release": null,
                "versions": [

                ]
            },
            {
                "name": "hammer",
                "platform": "Bower",
                "description": "A javascript library for multi-touch gestures :// You can touch this",
                "homepage": "http://hammerjs.github.io",
                "repository_url": "https://github.com/EightMedia/hammer.js.git",
                "normalized_licenses": [
                    "MIT"
                ],
                "rank": 14,
                "latest_release_published_at": "2016-04-22T16:14:36.000Z",
                "latest_release_number": "v2.0.8",
                "language": "JavaScript",
                "status": null,
                "package_manager_url": null,
                "stars": 15179,
                "forks": 2201,
                "keywords": [
                    "touch",
                    "gestures"
                ],
                "latest_stable_release": {
                    "id": 34179256,
                    "github_repository_id": 19730,
                    "name": "v2.0.8",
                    "sha": "ee611316bec077fcfbba3fd604ebc4b0b35ac288",
                    "kind": "tag",
                    "published_at": "2016-04-22T16:14:36.000Z",
                    "created_at": "2016-04-22T16:14:51.514Z",
                    "updated_at": "2016-04-22T16:14:51.514Z"
                },
                "versions": [

                ]
            }
        ];

        const mockStore = configureStore([thunkMiddleware])
        const store = mockStore({ dataUnsorted })
        const expectedActions = [{ type: 'FETCH_BOWER_SORTED', payload: { data, sort: 'DESC' } }]

        store.dispatch(sortResults(dataUnsorted, 'stars', 'ASC'))

        const actions = store.getActions()

        t.deepEqual(
            actions,
            expectedActions
        )
        resolve()

    })


});