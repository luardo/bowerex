# Clone of Bower IO search #
[https://bower.io/search/.](Link URL)

Developed in React + Redux
Used AVA for testing and AVA+Redux
Frontend FW is bootstrap from boostrap-react

Frontend design adjustments are not included due lack of time. I focused more in the functionality, however to make it look 1-1 to the original website should only take a couple more hours to finish it.


Instalation
npm install

Run 
npm start

Test
npm test
npm test -- --verbose