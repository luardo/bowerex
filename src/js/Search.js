import React from "react"

import SearchForm from "./components/SearchForm"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { clearResults, fetchBower, getGitHubUser, sortResults } from "./actions/searchActions"

import Results from "./components/Results"
import { Pagination } from 'react-bootstrap'

class Search extends React.Component {

    constructor() {
        super();
        this.state = {
            value: '',
            activePage: 1,
        };
    }

    //handles changes of search input text
    handleChange(e) {
        var self = this,
            keyword = e.target.value;

        if (keyword == '') { //if input is empty, clear results
            this.props.clearResults();
        }

        self.setState({
            value: keyword
        });

        setTimeout(function () {
            if (keyword.length >= 3) { //waits almost 1 sec to trigger  fetch action
                self.triggerFetch();
            }
        }, 900);

    }

    //triggers fetch data action
    triggerFetch(keyword) {
        if (this.props.fetching == false) { //prevents multiple calls, it waits until one call is finished
            this.props.fetchBower(this.state.value);
        }
    }

    //once bowerModules has been recieved from fetch, the owner name hast to be added into object
    componentWillReceiveProps(nextProps) {
        if (nextProps.owner == false) { //if owner is not set, call function to complete data
            this.props.getGitHubUser(nextProps.bowerModules);
        }
    }

    //handles page change
    handleSelect(eventKey) {
        this.setState({
            activePage: eventKey
        });
    }

    //sorts the results bowerModules based on field(name, owner, stars)
    sortResults(field) {
        const {bowerModules} = this.props;
        const {sort} = this.props;
        this.props.sortResults(bowerModules, field, sort);
    }

    render() {
        const {value} = this.state;
        const {bowerModules} = this.props;
        const {fetching} = this.props;
        var numberOfPages = bowerModules.length / 5;

        if (this.props.fetching == true) {
            numberOfPages = 0;
        }

        return (
            <div>
                <SearchForm Value={value} Change={this.handleChange.bind(this)} />
                <Results
                    results={bowerModules}
                    fetchStatus={this.props.fetching}
                    activePage={this.state.activePage}
                    searchWords={value}
                    onClick={this.sortResults.bind(this)} />

                <Pagination
                    bsSize="large"
                    items={numberOfPages}
                    activePage={this.state.activePage}
                    onSelect={this.handleSelect.bind(this)} />
                <br />

            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        bowerModules: state.bower.modules,
        sort: state.bower.sort,
        owner: state.bower.ownerSet,
        fetching: state.bower.fetching
    }
}

function mapDispatchToProps(dispatch) {
    return {
        clearResults: bindActionCreators(clearResults, dispatch),
        fetchBower: bindActionCreators(fetchBower, dispatch),
        getGitHubUser: bindActionCreators(getGitHubUser, dispatch),
        sortResults: bindActionCreators(sortResults, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)