import fetch from 'isomorphic-fetch'

/**
 * Async call to fetch results
 * @keyword keyword for search query
 */
export function fetchBower(keyword) {
    return (dispatch) => {
        dispatch({ type: "FETCH_BOWER" })

        return fetch('https://libraries.io/api/bower-search?q=' + keyword)
            .then(res => res.json())
            .then(data => dispatch({ type: "FETCH_BOWER_FULFILLED", payload: data }))
            .catch(err => dispatch({ type: "FETCH_BOWER_REJECTED", payload: err }))
    }
}

/**
 * Sorts the results
 * @data is the results array
 * @needle is the filter key 
 * @order is either ASC or DESC
 */
export function sortResults(data, needle, order) {
    return function (dispatch) {

        var orderByField = function (needle) {
            return function (a, b) {
                var a = a[needle],
                    b = b[needle];

                if (order == "ASC") {
                    if (a < b) {
                        return -1;
                    } else if (a > b) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else if (order == "DESC") {
                    if (a > b) {
                        return -1;
                    } else if (a < b) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            };
        };

        needle = orderByField(needle);
        var toggledOrder = ''

        if (order == "ASC") {
            toggledOrder = "DESC";
        } else {
            toggledOrder = "ASC";
        }

        dispatch({ type: "FETCH_BOWER_SORTED", payload: { data: data.sort(needle), sort: toggledOrder } })
    }
}

/**
 * Retreieve username from git based on git repo url
 * @data all object from search result
 */
export function getGitHubUser(data) {
    return function (dispatch) {
        for (var i = 0; i < data.length; i++) {
            var url = data[i].repository_url.replace('https://github.com/', '');
            data[i]['owner'] = url.split('/')[0];
        }
        dispatch({ type: "OWNER_UPDATED", payload: data })
    }
}

/**
 * Removes the results when the input text is empty
 */
export function clearResults() {
    return function (dispatch) {
        dispatch({ type: "CLEAR_RESULTS" })
    }
}