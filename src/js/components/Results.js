//results component 
import React from "react"
import { Grid, Row, Table, Thumbnail, Button, Col, Glyphicon } from 'react-bootstrap';
import Highlighter from './Highlighter'
require("../../scss/styles.scss");

export default class Results extends React.Component {

    render() {
        const resultsPerPage = 5;  //ToDo add this value as props 
        const {results} = this.props;
        const {activePage} = this.props;
        const {fetchStatus} = this.props;
        const {searchWords} = this.props;


        if (fetchStatus == true) { //if data is been fetched
            return (
                <div class="result container">
                    Fetching new results ...
                </div>
            );
        } else if (results.length > 0) { //when data has been retrieve and it has results
            const resultsList = results.map((result, index) => {
                if (index >= resultsPerPage * (activePage - 1) && index < activePage * resultsPerPage) {
                    return <tr key={index}>
                        <td className="name"><h4><a href={result.repository_url}>
                            {result.name}
                            <Highlighter
                                activeClassName="mark"
                                activeIndex= '-1'
                                highlightClassName="mark"
                                highlightStyle={{ fontWeight: 'bold' }}
                                searchWords={searchWords.split(/\s/).filter(word => word)}
                                textToHighlight={result.name}
                                />
                        </a>
                        </h4>
                            <small><a href={result.homepage}><Glyphicon glyph="home" /> {result.homepage}</a></small>
                            <p className="description">{result.description} </p></td>
                        <td className="hidden-xs">{result.owner} </td>
                        <td className="hidden-xs">{result.stars} </td>
                    </tr>
                }
            });

            return (
                <div class="result container">
                    <Table responsive>
                        <thead>
                            <tr>
                                <th><a href="#" onClick={() => { this.props.onClick('name') } }>Name</a></th>
                                <th className="hidden-xs"><a href="#" onClick={() => { this.props.onClick('owner') } }>Owner</a></th>
                                <th className="hidden-xs"><a href="#" onClick={() => { this.props.onClick('stars') } }>Stars</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            {resultsList}
                        </tbody>
                    </Table>

                </div>
            );
        } else { //when no data result is set
            return (
                <div class="result container">
                    Type the module you want to display
                </div>
            );
        }
    }
}