//search form input
import React from "react"
import { Grid, Row, Thumbnail, FormControl, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col } from 'react-bootstrap';

export default class SearchForm extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {Value} = this.props;
        return (
            <FormControl
            type="text"
            value={Value}
            placeholder="Enter text"
            onChange={this.props.Change}
          />
        );
    }
}