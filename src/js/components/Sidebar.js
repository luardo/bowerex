import React from "react"

export default class Sidebar extends React.Component {
    render() {
        return (
            <div className="list-group table-of-contents hidden-xs">
                <a className="list-group-item" href="#navbar">Home</a>
                <a className="list-group-item" href="#buttons">Creating Packages</a>
                <a className="list-group-item" href="#typography">API</a>
                <a className="list-group-item" href="#tables">Configurations</a>
                <a className="list-group-item" href="#forms">Tools</a>
                <a className="list-group-item" href="#navs">About</a>
            </div>
        );
    }
}