import React from "react"
import { IndexLink, Link } from "react-router";
import { Nav, NavItem, PageHeader } from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';

export default class Header extends React.Component {
    render() {
        return (
           <div className="header">
                    <div className="container">
                        <Nav bsStyle="pills" activeKey={1} pullRight >
                            <LinkContainer to={{ pathname: '/docs' }}>
                                <NavItem eventKey={1} href="/docs">Docs</NavItem>
                            </LinkContainer>
                            <LinkContainer to={{ pathname: '/search' }}>
                                <NavItem eventKey={2} href="/search">Search packages</NavItem>
                            </LinkContainer>
                            <LinkContainer to={{ pathname: '/blog' }}>
                                <NavItem eventKey={3} href="/blog">Blog</NavItem>
                            </LinkContainer>
                            <LinkContainer to={{ pathname: '/stats' }}>
                                <NavItem eventKey={4} href="/stats">Stats</NavItem>
                            </LinkContainer>
                        </Nav>

                        <PageHeader>Bower Search
                            <small>Cloned in React JS + Redux</small></PageHeader>
                    </div>
                </div>
        );
    }
}