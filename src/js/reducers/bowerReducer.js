export default function reducer(state = {
    modules: [],
    fetched: false,
    error: null,
    fetching: false,
    ownerSet: false,
    sort: 'ASC'
}, action) {

    switch (action.type) { 
        case "FETCH_BOWER": { //when fetch action starts but no modules data is yet retrieved
            return {...state, fetching: true, fetched: false }
        }
        case "FETCH_BOWER_FULFILLED": { //when fetch modules data has been complete
            return {
                ...state,
                fetched: true,
                fetching: false,
                ownerSet: false,
                modules: action.payload
            }
        }
        case "FETCH_BOWER_SORTED": { //when modules data has been sorted ASC or DESC
            return {
                ...state,
                fetched: true,
                sort: action.payload.sort,
                modules: action.payload.data
            }
        }

        case "OWNER_UPDATED": { //when owner has been added to modules object
            return {
                ...state,
                fetched: true,
                ownerSet: true,
                modules: action.payload
            }
        }
        case "CLEAR_RESULTS": { //clear the modules object
            return {
                ...state,
                fetched: true,
                fetching: false,
                ownerSet: false,
                modules: []
            }
        }
    }
    return state
}