import { combineReducers } from "redux"
import bower from "./bowerReducer"

//in this case was no need of combining reducers, but if I would like to add one more, it's better if its setup like this
export default combineReducers({
    bower
})