import React from "react"
import Header from "./components/Header"
import Sidebar from "./components/Sidebar"
require("../scss/styles.scss"); //custom style adjustments

export default class Layout extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <div className="col-lg-3 col-md-3 col-sm-4">
                        <Sidebar />
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-8">
                        <div className="row">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}