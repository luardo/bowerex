/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import ReactDOM from "react-dom";
import { browserHistory, Router, Route, IndexRoute, Link, withRouter } from 'react-router'
import { Provider } from "react-redux"
import Layout from "./js/Layout"
import Search from "./js/Search"
import store from "./js/store"

const app = document.getElementById('app');

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Layout} >
                <IndexRoute component={Search} />
                <Route path="/search" name="search" component={Search}></Route>
            </Route>
        </Router>
    </Provider>,
    app);